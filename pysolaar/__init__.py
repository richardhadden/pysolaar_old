__version__ = "0.7.0"

# Is this a horrible hack?
from .pysolaar import (
    PySolaar,
    PySolaarSearchSet,
    PySolaarError,
    fix_output_values,
    encode_id,
    decode_id,
    ID_SUBCLASS_SEPARATOR,
    FIELD_SUBCLASS_SEPARATOR,
    SOLR_TERM_REGEX,
)
