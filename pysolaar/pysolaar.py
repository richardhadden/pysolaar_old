from __future__ import annotations

import datetime
import functools
import json
from itertools import chain, islice
import re
from typing import Any, Callable, Dict, Generator, List, Set, Tuple, Type, Union

import pysolr  # type: ignore


from .utils.camel_to_snake import camelToSnake

# This string is used to separate subclass and ID of an entity in Solr,
# so we can avoid potential ID clashes. If it turns out that this
# string is used for something (doesn't seem likely), change it here
ID_SUBCLASS_SEPARATOR = "#_#_#_#_#"
FIELD_SUBCLASS_SEPARATOR = "______"

SOLR_TERM_REGEX = r"[A-Za-z0-9\_]+:"
URL_REGEX_STRING = r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"
URL_REGEX = re.compile(URL_REGEX_STRING)


def chunks(iterable, size=10) -> Generator:
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, size - 1))


class PySolaarError(Exception):
    pass


def encode_fieldname(subclass, field_name) -> str:
    return f"{camelToSnake(subclass)}{FIELD_SUBCLASS_SEPARATOR}{field_name}"


def encode_id(subclass, id_value) -> str:
    return f"{camelToSnake(subclass)}{ID_SUBCLASS_SEPARATOR}{id_value}"


def decode_id(solr_id_string: str) -> str:
    # print(solr_id_string)
    return solr_id_string.split(ID_SUBCLASS_SEPARATOR)[-1]


def build_sort_statement(classname, order_field):
    order = "asc"
    if order_field.startswith("-"):
        order_field = order_field[1:]
        order = "desc"

    order_field = (
        encode_fieldname(classname, order_field) if order_field != "id" else order_field
    )
    return f"{order_field} {order}"


def fix_output_values(
    value: Union[str, datetime.datetime],
    key=None,
    single_value_fields=set(),
    store_as_json_fields=set(),
) -> Any:
    # print("KEY", key)
    # TODO: do we actually want to do this? REALLY, we should just unpack
    # single instances of things that *ARE* single values, not *EVERYTHING*
    # otherwise it'll be a pain to iterate over it.
    if False:
        pass

    # Unpack single-length lists to just the value (as would expect, non?)
    elif isinstance(value, list) and len(value) == 1 and key in single_value_fields:
        value = value[0]
    # If actual list, call function on each item of the list
    elif isinstance(value, list) and len(value) > 1:
        value = [fix_output_values(v) for v in value]

    # Unpack json back to whatever it was...
    if key in store_as_json_fields and isinstance(value, list):
        value = json.loads(value[0])

    # Check the type as well as the key, to satisfy mypy!
    if isinstance(value, str) and key and remove_prefix_from_key(key) == "id":
        # If we're dealing with ID, we need to remove the subclass prefix
        # by splitting on our highly unique separator
        value = decode_id(value)
    elif isinstance(value, str):
        possible_datetime = pysolr.DATETIME_REGEX.search(value)
        if possible_datetime:
            date_values = possible_datetime.groupdict()

            for dk, dv in date_values.items():
                date_values[dk] = int(dv)

            value = datetime.datetime(
                date_values["year"],
                date_values["month"],
                date_values["day"],
                date_values["hour"],
                date_values["minute"],
                date_values["second"],
            )

    return value


def remove_prefix_from_key(key):
    if FIELD_SUBCLASS_SEPARATOR in key:
        return key.split(FIELD_SUBCLASS_SEPARATOR)[1]
    else:
        return key


def results_class_factory(factory_single_value_fields, store_as_json_fields):
    # print("fsvf", factory_single_value_fields)

    class Results(pysolr.Results):
        def __init__(self, decoded: Dict, next_page_query: Callable = None) -> None:
            super().__init__(decoded, next_page_query=next_page_query)
            # print(decoded)
            self.raw_response = decoded
            # main response part of decoded Solr response
            response_part = decoded.get("response") or {}
            self.docs = self.fix_docs(response_part.get("docs", ()))
            self._num_found = response_part.get("numFound", 0)

        def count(self):
            return self._num_found

        def fix_docs(self, docs: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
            """Fix some irritating issues with docs (e.g. one-item lists),
            no date-time conversion, etc."""
            fixed_docs: List = []

            for doc in docs:

                fixed_doc = {
                    remove_prefix_from_key(key): fix_output_values(
                        value,
                        key=key,
                        single_value_fields=factory_single_value_fields,
                        store_as_json_fields=store_as_json_fields,
                    )
                    for key, value in doc.items()
                    if key != "_doc"
                }

                if doc.get("_doc"):
                    # print("doc", doc.get("_doc"))
                    for subdoc in doc.get("_doc"):

                        # print("SD", subdoc)
                        parent_class, nested_class, field_name = subdoc.pop(
                            "pysolaar_type"
                        )[0].split("__")
                        if field_name not in fixed_doc:
                            fixed_doc[field_name] = []

                        fixed_subdoc = {
                            remove_prefix_from_key(key): fix_output_values(
                                value,
                                key=f"{nested_class}{FIELD_SUBCLASS_SEPARATOR}{key}"
                                if key not in {"id"}
                                else key,
                                single_value_fields=factory_single_value_fields,
                            )
                            for key, value in subdoc.items()
                            if key not in {"_docs", "_version_"}
                        }

                        fixed_doc[field_name].append(fixed_subdoc)

                    doc.pop("_doc")

                fixed_docs.append(fixed_doc)
            return fixed_docs

    return Results


class PySolaar:
    """Base class for PySolr wrapper.
    This class should:

    ✅ Keep track of subclasses (the actual classes we wish to put into Solr)
    ✅ Probably take a PySolr object at its init (or address or other options)
        - Do we want this being init-ed? ... no???
        - Added a configure_solr method instead
    - Provide a subclass-implementable method for getting data to index
        - with a @type relating to the subclass
    ✅ Update
    """

    __subclasses: List[Type] = []
    _solr: pysolr.Solr = None
    default_search_field: str

    @classmethod
    @property
    def pysolaar_classes(cls) -> List[Type]:
        """ Gets all the subclasses; for external use only probably """
        return cls.__subclasses

    @classmethod
    def _RESET(cls) -> None:
        """Method used by pytest fixture to reset the listed
        subclasses between each test.
        """
        cls.__subclasses = []
        cls._solr = None

    def __init_subclass__(cls) -> None:
        """Adds subclass to __subclasses list when it is declared

        (Odd... you'd think from name of __init_subclass__ it would
        be called by actually initialising a subclass, but it seems that
        just declaring it does the job ... which is what we want here.)
        """
        super().__init_subclass__()

        # Check that class implements cls.get_index_set() method;
        # We want to do this manually at declaration time,
        # not at runtime by some placeholder that raises error
        if not hasattr(cls, "get_index_set"):
            raise NotImplementedError(
                f"PySolaar class <{cls.__name__}> needs to implement 'get_index_set'"
                " method, returning an interable of docs (e.g. as dict) to index."
            )

        # If we have it, probably should do some sanity checks like,
        # is it callable; but for now, we need to make this method
        # into a class method.
        cls.get_index_set = classmethod(cls.get_index_set)  # type: ignore
        # And then append it to our list of subclasses.

        if hasattr(cls, "get_single_object"):
            cls.get_single_object = classmethod(cls.get_single_object)  # type: ignore

        cls.__subclasses.append(cls)

    def __init__(self) -> None:
        super().__init__()

    @classmethod
    def _get_boosts(cls):
        boosts = {}
        for subclass in cls.__subclasses:
            if hasattr(subclass, "boost"):
                for field, value in subclass.boost.items():
                    boosts[encode_fieldname(subclass.__name__, field)] = value
        return boosts

    @classmethod
    def clean_index_set(
        cls,
        index_set: Dict[Any, Any],
        subclass,
        is_nested=False,
        orig_subclass="",
        parent_key=None,
    ) -> Dict[Any, Any]:
        # print(">>>> ", index_set)
        cleaned_index_set: Dict[Any, Any] = {}
        for key, value in index_set.items():

            if isinstance(value, datetime.datetime):
                value = value.isoformat() + "Z"  # Needs timezone
            elif isinstance(value, datetime.date):
                value = (
                    value.isoformat() + "T00:00:00Z"
                )  # Needs Thours:mins:seconds+timezone
            elif is_nested and key == "id":

                value = encode_id(
                    orig_subclass.__name__,
                    encode_id(parent_key, encode_id(key, value)),
                )

            elif key == "id":
                value = encode_id(subclass.__name__, value)

            if hasattr(subclass, "store_as_json") and key in getattr(
                subclass, "store_as_json"
            ):
                value = json.dumps(value)

            if key == "id":
                cleaned_index_set[key] = value
            elif key == "@id":
                # cleaned_index_set[key] = value
                cleaned_index_set["id"] = value
            elif isinstance(value, dict):
                # print("isDICT")
                nested = cls.clean_index_set(
                    {
                        f"{key}__{dict_key}": dict_value
                        for dict_key, dict_value in value.items()
                    },
                    subclass=subclass,
                    is_nested=is_nested,
                )
                # print("nested", nested)
                cleaned_index_set = {**cleaned_index_set, **nested}
            elif hasattr(subclass, "nested_document_fields") and key in getattr(
                subclass, "nested_document_fields"
            ):
                if "_doc" not in cleaned_index_set:
                    cleaned_index_set["_doc"] = []

                nested_class, identifiers = value

                for identifier in identifiers:

                    nested_single_object = nested_class.get_single_object(identifier)
                    if isinstance(nested_single_object, (list, Generator, tuple)):
                        # print("nested field is a list")
                        for nested_subdoc in nested_single_object:
                            nested_doc = cls.clean_index_set(
                                nested_subdoc,
                                nested_class,
                                is_nested=True,
                                orig_subclass=subclass,
                                parent_key=key,
                            )
                            nested_doc[
                                "pysolaar_type"
                            ] = f"{camelToSnake(subclass.__name__)}__{camelToSnake(nested_class.__name__)}__{key}"
                            # print("nested_doc", nested_doc)
                            cleaned_index_set["_doc"].append(nested_doc)
                    else:
                        # print("nested_field is a single")
                        nested_doc = cls.clean_index_set(
                            nested_single_object,
                            nested_class,
                            is_nested=True,
                            orig_subclass=subclass,
                            parent_key=key,
                        )
                        nested_doc[
                            "pysolaar_type"
                        ] = f"{camelToSnake(subclass.__name__)}__{camelToSnake(nested_class.__name__)}__{key}"

                        cleaned_index_set["_doc"].append(nested_doc)
            elif is_nested:
                # print("is_nested", key, value)
                cleaned_index_set[key] = value
            else:
                cleaned_index_set[encode_fieldname(subclass.__name__, key)] = value
        # print("--", cleaned_index_set)
        return cleaned_index_set

    @classmethod
    def get_data_to_index(cls, key=None) -> Generator:
        """Generator functions to yield documents to push to Solr.

        If called by the main PySolaar instance, push everything (top if-clause)
        If called with a key, on a subclass, just update that subclass (elif key)
        Otherwise, if called on a subclass, update that subclass.j

        The yield from subclass stuff is there, as subclasses need to be pushed
        first as standalone to Solr as separate classes to avoid field-not-found errors (??)

        No idea why this happens --

        Problem with this hack is duplicating the nested fields three times
        1) initial push as standalone
        2) standalone caused by pushing as nested
        3) nested as nested..."""

        # If this method is called on the main PySolaar class,
        # update all the subclasses
        if cls.__name__ == "PySolaar":

            # Iterate the subclasses...
            for subclass in cls.__subclasses:  # sorted(
                # cls.__subclasses, key=lambda cl: cl.__name__ in nested_classes
                # ):

                # Call each subclass's get_index_set method, and tack on
                # the subclass's name, which we will use to filter results
                # to a particular class in Solr

                for index_set in subclass.get_index_set():
                    docs = cls.clean_index_set(index_set, subclass)
                    try:
                        subdocs = docs["_doc"]
                        # print("SUBDOCS", subdocs)
                        yield from subdocs
                    except KeyError:
                        pass
                    yield {
                        "pysolaar_type": camelToSnake(subclass.__name__),
                        **docs,
                    }

        # Otherwise, if called specifically on a subclass, just update
        # that subclass
        elif key:
            if hasattr(cls, "get_single_object"):
                doc = cls.clean_index_set(cls.get_single_object(key), cls)  # type: ignore
                try:
                    subdocs = doc["_doc"]
                    # print("SUBDOCS", subdocs)
                    yield from subdocs
                except KeyError:
                    pass

                yield {
                    "pysolaar_type": camelToSnake(cls.__name__),
                    **doc,
                }

            else:
                raise PySolaarError(
                    "To update single objects, you must implement a "
                    "get_single_object method on your PySolaar subclass"
                )

        else:
            for index_set in cls.get_index_set():  # type: ignore
                docs = cls.clean_index_set(index_set, cls)
                try:
                    subdocs = docs["_doc"]
                    # print("SUBDOCS", subdocs)
                    yield from subdocs
                except KeyError:
                    pass

                yield {
                    "pysolaar_type": camelToSnake(cls.__name__),
                    **docs,
                }

    @classmethod
    def configure_solr(cls, url: str, **kwargs) -> None:
        """Class method to just pass through PySolr configuration options
            exactly as PySolr expects them:

            From pysolr.Solr (https://github.com/django-haystack/pysolr/blob/master/pysolr.py, 343)

            def __init__(
                self,
                url,
                decoder=None,
                encoder=None,
                timeout=60,
                results_cls=Results,
                search_handler="select",
                use_qt_param=False,
                always_commit=False,
                auth=None,
                verify=True,
                session=None,
            ):
                self.decoder = decoder or json.JSONDecoder()
                self.encoder = encoder or json.JSONEncoder()
                self.url = url
                self.timeout = timeout
                self.log = self._get_log()
                self.session = session
                self.results_cls = results_cls
                self.search_handler = search_handler
                self.use_qt_param = use_qt_param
                self.auth = auth
                self.verify = verify
                self.always_commit = always_commit

        Helpfully, PySolr doesn't document what these defaults are, but I will.
        """

        cls._solr = pysolr.Solr(
            url,
            always_commit=kwargs.pop("always_commit")
            if "always_commit" in kwargs
            else True,
            # results_cls=results_class_factory(single_value_fields),
            **kwargs,
        )
        cls._setup_results_class()

    @classmethod
    def encode_return_fields(cls):
        if hasattr(cls, "return_fields") and isinstance(
            getattr(cls, "return_fields"), dict
        ):
            encoded_return_fields = {}

            for set_name, fields in getattr(cls, "return_fields").items():
                encoded_return_fields[set_name] = ["id"] + [
                    encode_fieldname(cls.__name__, field) for field in fields
                ]
            return encoded_return_fields

        elif hasattr(cls, "return_fields") and isinstance(
            getattr(cls, "return_fields"), (list, tuple)
        ):
            return ["id"] + [
                encode_fieldname(cls.__name__, field)
                for field in getattr(cls, "return_fields")
            ]

        return None

    @classmethod
    def encode_additional_args(cls) -> Dict:
        additional_args = {}

        if hasattr(cls, "default_order_by"):
            if isinstance(getattr(cls, "default_order_by"), str):
                additional_args["sort"] = build_sort_statement(
                    cls.__name__, getattr(cls, "default_order_by")
                )
            elif isinstance(getattr(cls, "default_order_by"), (list, tuple)):
                additional_args["sort"] = ",".join(
                    build_sort_statement(cls.__name__, o)
                    for o in getattr(cls, "default_order_by")
                )

        return additional_args

    @classmethod
    def _setup_results_class(cls):
        # So here, when we configure, we need to get all the single field names...
        single_value_fields = {"id"}
        store_as_json_fields = set()
        for subclass in cls.__subclasses:
            # print("subclass", subclass)
            if hasattr(subclass, "single_value_fields"):
                for field_name in getattr(subclass, "single_value_fields"):
                    single_value_fields.add(
                        encode_fieldname(subclass.__name__, field_name)
                    )
            if hasattr(subclass, "store_as_json"):
                for field_name in getattr(subclass, "store_as_json"):
                    store_as_json_fields.add(
                        encode_fieldname(subclass.__name__, field_name)
                    )
        cls._solr.results_cls = results_class_factory(
            single_value_fields, store_as_json_fields
        )

    @classmethod
    def update(cls, key=None, max_chunk_size=1000) -> None:
        if cls._solr is None:
            raise PySolaarError(
                "You must call PySolaar.configure_solr(<URL>) with Solr URL "
                "before calling PySolaar.update() or running any searches."
            )
        boost = cls._get_boosts()
        cls._setup_results_class()
        # Instead of getting all the data as one list, grab chunks of it
        # and push chunk at a time
        for data in chunks(cls.get_data_to_index(key=key), size=max_chunk_size):
            # Turn each chunk to a list
            data = list(data)
            # print(data)
            if boost:
                cls._solr.add(data, commit=True, boost=boost)  # skip boost
            else:
                cls._solr.add(data, commit=True)

    @classmethod
    def items(
        cls, identifiers: List, transform_identifier=lambda x: x
    ) -> Tuple[Type[PySolaar], str]:

        return cls, transform_identifier([transform_identifier(i) for i in identifiers])

    @classmethod
    def all(cls) -> PySolaarSearchSet:
        return PySolaarSearchSet(
            search="*:*",
            pysolaar_type=cls.__name__,
            pysolr_instance=cls._solr,
            return_fields=cls.encode_return_fields(),
            nested_document_fields=getattr(cls, "nested_document_fields", set()),
            **cls.encode_additional_args(),
        )

    @classmethod
    def filter(cls, **kwargs) -> PySolaarSearchSet:

        # if "id" in kwargs:
        #    kwargs["id"] = encode_id(cls.__name__, kwargs["id"])

        return PySolaarSearchSet(
            search="*:*",
            pysolaar_type=cls.__name__,
            pysolr_instance=cls._solr,
            return_fields=cls.encode_return_fields(),
            nested_document_fields=getattr(cls, "nested_document_fields", set()),
            fqs=kwargs,
            **cls.encode_additional_args(),
        )

    @classmethod
    def search(cls, search, **kwargs) -> PySolaarSearchSet:

        # Check default search field is set, or manually set in Solr query.
        # Otherwise throw error.
        if not ":" in search and not hasattr(cls, "default_search_field"):
            raise PySolaarError(
                "A search must either specify a search field"
                " using Solr syntax ('field:searchterm'), or "
                "the PySolaar class must set a field as default."
            )

        # If we just have a search term with no specified field,
        # set the default field on its
        if not ":" in search:
            search = f"{cls.default_search_field}:{search}"

        # If ":" in search and... looks like a URL...
        elif URL_REGEX.match(search):
            escaped_url = search.replace(":", r"\:")

            search = f'{cls.default_search_field}:"{escaped_url}"'

        """
        n.b the above, and below: we're trying to escape a colon in, say, a URL if the user just does
        Thing.search("http://whatever.com"). If there's a more complex search string, e.g. a full Solr
        query with the field specified, Thing.search("website:http://whatever.net"), the user should
        do all the escaping themselves.
        """

        # If there's a manually-entered Solr search string with an id: in,
        # we need to prefix the id value with the class and separator.
        if re.search(r"^id:", search):
            search = re.sub(
                r"^id:",
                f"id:{camelToSnake(cls.__name__)}{ID_SUBCLASS_SEPARATOR}",
                search,
            )
        elif re.search(r" id:", search):
            search = re.sub(
                r" id:",
                f" id:{camelToSnake(cls.__name__)}{ID_SUBCLASS_SEPARATOR}",
                search,
            )

        """
        Just to explain the above, as it confused me quite a lot after not looking
        at it for a while:

        If we have a manual SOLR search string that contains an id:

            'name:john AND id:big_john'

        we need to modify the id part of the string to include the subclass,
        by replacing 'id:' with 'id:person#_#_#_#_#big_john'

        [I *think* we need both regexes, in order to tackle the id at the beginning of the 
        string *and* the id in the middle, while not falling prey to "aphid:something", i.e. a
        field name that happens to end with 'id']

        """

        field_subclass_prefix = camelToSnake(cls.__name__ + FIELD_SUBCLASS_SEPARATOR)

        # SO... here, we go through all the colon-separated parts, assuming fields
        # have sensible names (chars, numbers, separators) and replace each with
        # a prefixed version... THIS is obviously stupid: modifying SOLR query
        # statements and then pretending we aren't... but it ain't broke yet...
        for field in re.findall(SOLR_TERM_REGEX, search):
            # print(field)
            if not field.startswith("id:"):
                search = re.sub(field, field_subclass_prefix + field, search)

        search_object = PySolaarSearchSet(
            search=search,
            pysolaar_type=cls.__name__,
            pysolr_instance=cls._solr,
            return_fields=cls.encode_return_fields(),
            nested_document_fields=getattr(cls, "nested_document_fields", set()),
            **cls.encode_additional_args(),
            **kwargs,
        )

        return search_object


class PySolaarSearchSet:
    """Class returned by calling .search() on a PySolaar object.

    This class has two functions:
        - A search and chainable 'filter()' methods (and others of the same nature perhaps?),
          returning a new instance of the class in each case, à la Django query objects.
          Translates all the search and filters into PySolr syntax.
        - As a proxy object to a PySolr Result object, which is not
          evaluated until one of the proxy methods is called (e.g. )

    (All the crap at the bottom relates to the methods which should serve as proxies to
    the results, and which are the SearchSet)
    """

    def __init__(
        self,
        search: str = "*:*",
        pysolr_instance: pysolr.Solr = pysolr.Solr("NONE"),
        pysolaar_type: str = "None",
        return_fields=None,
        fqs: Dict = {},
        nested_document_fields: Set = set(),
        **kwargs,
    ) -> None:

        self._solr = pysolr_instance
        self._pysolaar_type = pysolaar_type
        self._search = search
        self._fqs = fqs or {}
        self._add_type_to_fqs()
        self._kwargs = kwargs
        self._nested_document_fields = nested_document_fields
        self._return_fields = return_fields

        # self._defer_get_results = lazy_object_proxy.Proxy(self._get_results)
        self._results: dict = {}
        super().__init__()

    def _add_type_to_fqs(self) -> None:
        if "pysolaar_type" not in self._fqs:
            self._fqs["pysolaar_type"] = camelToSnake(self._pysolaar_type)

    # ------------------------------------------------------------
    # Here are all the tricky methods for chaining this class, proxying, etc.
    # (See class doc)
    # ------------------------------------------------------------

    def _new_method_proxy(func: Callable) -> Callable:  # type: ignore
        def inner(self, *args, **kwargs):

            if not self._results:
                self._results = self._get_results()
            return func(self._results, *args, **kwargs)

        return inner

    def __setattr__(self, name: str, value: str) -> None:
        """Hack attr setter to set on proxy'd _result object"""
        if name in {
            "_solr",
            "_pysolaar_type",
            "_search",
            "_fqs",
            "_kwargs",
            "_results",
            "_count",
            "_return_fields",
            "_nested_document_fields",
        }:
            self.__dict__[name] = value
        else:
            if not self._results:
                self.__dict__["_results"] = self._get_results()
            self.__dict__["results"][name] = value

    def __delattr__(self, name: str) -> None:
        if name == "_results":
            raise TypeError("can't delete _results.")
        if not self._results:
            self._results = self._get_results()
        delattr(self.__dict__["_results"], name)

    def __getattr__(self, name) -> Any:
        if name in {
            "filter",
            "_build_new_search_object",
            "first",
        }:
            return self.__dict__[name]()

        if not self._results:
            self._results = self._get_results()
        return getattr(self._results, name)

    def __repr__(self) -> str:

        if not self._results:
            self._results = self._get_results()
        return "PySolaarSearchObject"
        terms: set
        try:
            terms = {
                self._search.split(FIELD_SUBCLASS_SEPARATOR)[1],
                self._map_fqs_to_unprefixed_fq(),
            }
        except:
            terms = set()

        return (
            f"{self.__dict__['_results']} ({len(self.__dict__['_results'])} results)"
            f" for {self.__dict__['_pysolaar_type']}SolrSearchObject"
            f" [{terms}]>"
        )

    def __len__(self) -> int:
        if not self._results:
            self._results = self._get_results()

        # Changed from using self._results.hits as can paginate
        return len(self._results)

    __bytes__: Callable = _new_method_proxy(bytes)
    __bool__: Callable = _new_method_proxy(bool)
    __iter__: Callable = _new_method_proxy(iter)

    # ------------------------------------------------------------
    # Here are public-facing methods for chaining queries, etc.
    # ------------------------------------------------------------

    def filter(self, **kwargs: Dict[str, Any]) -> PySolaarSearchSet:
        """Adds keyword args, e.g. text="something" to object and returns
        a new instance.
        """

        new_kwargs = {}

        # if "id" in kwargs:
        #    new_kwargs["id"] = encode_id(self._pysolaar_type, kwargs["id"])

        return PySolaarSearchSet(
            pysolr_instance=self._solr,
            pysolaar_type=self._pysolaar_type,
            search=self._search,
            return_fields=self._return_fields,
            fqs={**self._fqs, **kwargs, **new_kwargs},  # Create new, updating fqs
            nested_document_fields=self._nested_document_fields,
            **self._kwargs,
        )

    def order_by(self, *args) -> PySolaarSearchSet:
        if len(args) == 0:
            raise PySolaarError(
                ".order_by method must be called with the name of a single-value field"
            )

        orderings = "id asc"

        if len(args) == 1 and isinstance(args[0], str):
            orderings = build_sort_statement(self._pysolaar_type, args[0])

        elif len(args) == 1 and isinstance(args[0], (list, tuple)):
            orderings = ",".join(
                build_sort_statement(self._pysolaar_type, o) for o in args[0]
            )

        else:
            orderings = ",".join(
                build_sort_statement(self._pysolaar_type, o) for o in args
            )

        self._kwargs["sort"] = orderings

        return PySolaarSearchSet(
            pysolr_instance=self._solr,
            pysolaar_type=self._pysolaar_type,
            search=self._search,
            return_fields=self._return_fields,
            fqs=self._fqs,
            nested_document_fields=self._nested_document_fields,
            **self._kwargs,
        )

    def exclude(self, **kwargs) -> PySolaarSearchSet:
        pass

    def first(self) -> Union[Dict[str, Any], None]:
        """Returns the first item in the results"""
        if not self._results:
            self._results = self._get_results()
        try:
            return self._results.docs[0]
        except IndexError:
            return None

    def count(self) -> int:
        self._count = self._get_results(rows=0).count()
        return self._count

    def paginate(self, start=0, n=10) -> PySolaarSearchSet:

        return PySolaarSearchSet(
            pysolr_instance=self._solr,
            pysolaar_type=self._pysolaar_type,
            search=self._search,
            return_fields=self._return_fields,
            fqs=self._fqs,
            nested_document_fields=self._nested_document_fields,
            **self._kwargs,
            **{"start": start, "rows": n},
        )

    def iterator(self, chunk_size=100, group_chunks=False) -> Generator:
        """For large result sets, grabs n docs at a time from Solr and
        makes them accessible as an iterator of individual documents.
        (Not iterator of result sets!)
        """
        current_pos = 0
        current_result = self._get_results(rows=chunk_size, start=current_pos)
        while current_result:
            if group_chunks:
                yield current_result
            else:
                yield from current_result
            current_pos += chunk_size
            current_result = self._get_results(rows=chunk_size, start=current_pos)

    def return_fields(self, field_group):
        # If a list is provided, select just those fields
        if isinstance(field_group, (list, tuple, set)):
            self._kwargs["fl"] = (
                ",".join(
                    [
                        encode_fieldname(self._pysolaar_type, field)
                        for field in field_group
                    ]
                )
                + ",id"
            )
        elif field_group == "__all__":
            self._kwargs["fl"] = "*"

        elif field_group and isinstance(self._return_fields, (list, tuple, set)):
            raise PySolaarError(
                "If a single Return Fields set is provided, it is the default."
                " Use '__all__' to override or specify fields as an iterable."
            )
        # Otherwise use one of the 'return_field' sets
        elif self._return_fields:

            if field_group in self._return_fields:
                fields_to_return = self._return_fields[field_group]
                self._kwargs["fl"] = ",".join(fields_to_return)

            else:
                raise PySolaarError(
                    f"Field return group '{field_group}' not found in return_fields for {self._pysolaar_type}"
                )
        else:
            raise PySolaarError(
                "To use the 'return_fields' modifier, a 'return_fields' variable must be set on your PySolaar class."
            )
        # print("FL", self._kwargs["fl"])
        return PySolaarSearchSet(
            pysolr_instance=self._solr,
            pysolaar_type=self._pysolaar_type,
            search=self._search,
            return_fields=self._return_fields,
            fqs=self._fqs,
            nested_document_fields=self._nested_document_fields,
            **self._kwargs,
        )

    # ------------------------------------------------------------
    # Here are methods for translating query object to PySolr
    # and running the query.
    # ------------------------------------------------------------

    def _get_results(self, rows=100000, start=0) -> pysolr.Results:
        """Assembles Solr query, runs, and returns PySolr Results object.

        This method is called by invoking various methods (iter, len, etc.)
        on the class, which sets the result.


        """

        # If some other function has not already turned 'self._return_fields' into fl,
        # add it in now — either using 'default' from dict or just list
        if self._return_fields and "fl" not in self._kwargs:
            try:
                fields_to_return = self._return_fields["default"]
            except:
                fields_to_return = self._return_fields
            self._kwargs["fl"] = ",".join(fields_to_return)

        if self._solr is None:  # If Solr not configured, raise error
            raise PySolaarError(
                "You must call PySolaar.configure_solr(<URL>) with Solr URL "
                "before calling PySolaar.update() or running any searches."
            )
        # print("SEARCH", self._search)
        rows_to_get = self._kwargs.pop("rows", None) or rows
        start_position = self._kwargs.pop("start", None) or start
        results = self._solr.search(
            self._search,
            fq=self._map_fqs_to_pysolr_fq(),
            rows=rows_to_get,
            start=start_position,
            **self._kwargs,
        )

        return results

    def _map_fqs_to_unprefixed_fq(self) -> List[str]:
        return [f"{key}={value}" for key, value in self._fqs.items()]

    def _map_fqs_to_pysolr_fq(self) -> List[str]:

        query: List = []
        for key, value in self._fqs.items():
            query_term = self._map_single_fq_to_query_form(
                key,
                value,
                ps_type=self._pysolaar_type,
                nested_document_fields=self._nested_document_fields,
            )
            query.append(query_term)

        return query

    @staticmethod
    def _map_single_fq_to_query_form(
        key,
        value,
        ps_type="missingtype",
        nested=False,
        ps_type_if_nested=None,
        m1=None,
        nested_document_fields=set(),
    ) -> str:
        print(key, value)
        """
        Things to deal with here:

        key=value
        key__gt=value
        nestedVarName__key=value
        nestedVarName__key__gt=value

        """

        if isinstance(value, str) and ":" in value:
            value = f'"{value}"'

        if key == "id":
            if nested:

                return f"{key}:{ps_type_if_nested}#_#_#_#_#{m1}#_#_#_#_#id#_#_#_#_#cOne"
            return f"{key}:{camelToSnake(ps_type)}{ID_SUBCLASS_SEPARATOR}{value}"

        if key == "pysolaar_type":
            return f"{key}:{value}"

        # If nothing special about it, just return key-value pair
        if "__" not in key:

            return f"{encode_fieldname(ps_type, key) if ps_type else key}:{value}"

        key, *modifier = key.split("__")

        # It appears that Solr needs all dates in queries to be in the form
        # YYYY-MM-DDThh:mm:ssZ otherwise it won't work.
        # No idea why: it's happy to parse dates when input as basically anything
        # but not this... :~

        # TODO: figure out some way of avoiding this dirty hack
        if isinstance(value, datetime.datetime):
            value = value.isoformat() + "Z"  # Needs timezone
        elif isinstance(value, datetime.date):
            value = (
                value.isoformat() + "T00:00:00Z"
            )  # Needs Thours:mins:seconds+timezone

        if modifier[0] == "lt":
            return (
                f"{encode_fieldname(ps_type, key) if ps_type else key}:[* TO {value}}}"
            )
        if modifier[0] == "lte":
            return (
                f"{encode_fieldname(ps_type, key) if ps_type else key}:[* TO {value}]"
            )
        if modifier[0] == "gt":
            return (
                f"{encode_fieldname(ps_type, key) if ps_type else key}:{{{value} TO *]"
            )
        if modifier[0] == "gte":
            return (
                f"{encode_fieldname(ps_type, key) if ps_type else key}:[{value} TO *]"
            )

        if modifier[0] == "exact":
            return f"{key}:{value}"

        if key in nested_document_fields:
            try:
                m = "__".join(modifier)
                m1, *m2 = m.split("__")

                nested_search_part = PySolaarSearchSet._map_single_fq_to_query_form(
                    m,
                    value,
                    ps_type="",
                    nested=True,
                    ps_type_if_nested=camelToSnake(ps_type),
                    m1=key,
                )

                query = (
                    f'{{!parent which="*:* -_nest_path_:* +pysolaar_type:{camelToSnake(ps_type)}"}}'  # Parent, whose type matches the class
                    f"(+_nest_path_:\/_doc +pysolaar_type:{camelToSnake(ps_type)}__*__{key} +{nested_search_part})"
                )
                # print("Q", query)
                return query
            except:
                pass
                # raise PySolaarError("Error with query on nested field.")
        else:
            try:
                m = "__".join(modifier)
                m1, *m2 = m.split("__")
                n = PySolaarSearchSet._map_single_fq_to_query_form(
                    m, value, ps_type="", m1=key
                )
                query = f"{encode_fieldname(ps_type, key) if ps_type else key}__{n}"
                # print("QUERY", query)
                return query
            except:
                raise PySolaarError("Error with query on nested field.")
