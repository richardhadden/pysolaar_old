import pytest

import pysolr

from pysolaar import __version__
from pysolaar import (
    PySolaar,
    PySolaarSearchSet,
    PySolaarError,
    FIELD_SUBCLASS_SEPARATOR,
)


def test_can_init():
    s = PySolaarSearchSet(pysolaar_type="Person")
    assert s._pysolaar_type == "Person"
    assert s._fqs == {"pysolaar_type": "person"}


def test_can_chain():
    a = PySolaarSearchSet(pysolaar_type="Person")
    b = a.filter(something="dog")
    assert type(b) is PySolaarSearchSet
    assert b._fqs == {"pysolaar_type": "person", "something": "dog"}
    c = b.filter(something_else="cat")
    assert type(c) is PySolaarSearchSet
    assert c._fqs == {
        "pysolaar_type": "person",
        "something": "dog",
        "something_else": "cat",
    }

    # And check the original objects are still good
    assert a is not b
    assert b is not c
    assert a._fqs == {"pysolaar_type": "person"}
    assert b._fqs == {"pysolaar_type": "person", "something": "dog"}


def test_map_fqs_to_query_form():
    # Alias this for typing ease
    m = PySolaarSearchSet._map_single_fq_to_query_form

    # A plain field-value pair needs to be just colon-concatenated
    assert (
        m("something", "dog") == f"missingtype{FIELD_SUBCLASS_SEPARATOR}something:dog"
    )

    """ This error exception now difficult, as almost anything could be something"""
    # If nothing matches, raise an error
    # with pytest.raises(PySolaarError) as err:
    #    assert m("something__missing__gt", "value")

    # Numeric comparisons need converting to range queries
    assert (
        m("number__lt", 3) == f"missingtype{FIELD_SUBCLASS_SEPARATOR}number:[* TO 3}}"
    )
    assert (
        m("number__lte", 3) == f"missingtype{FIELD_SUBCLASS_SEPARATOR}number:[* TO 3]"
    )
    assert (
        m("number__gt", 3) == f"missingtype{FIELD_SUBCLASS_SEPARATOR}number:{{3 TO *]"
    )
    assert (
        m("number__gte", 3) == f"missingtype{FIELD_SUBCLASS_SEPARATOR}number:[3 TO *]"
    )

    assert m(
        "nested__something", "dog", "ParentThing", nested_document_fields={"nested"}
    ) == (
        '{!parent which="*:* -_nest_path_:* +pysolaar_type:parent_thing"}'
        "(+_nest_path_:\/_doc +pysolaar_type:parent_thing__*__nested +something:dog)"
    )

    assert m(
        "nested__something__gt", 3, "ParentThing", nested_document_fields={"nested"}
    ) == (
        '{!parent which="*:* -_nest_path_:* +pysolaar_type:parent_thing"}'
        f"(+_nest_path_:\/_doc +pysolaar_type:parent_thing__*__nested +something:{{3 TO *])"
    )

    assert (
        m("nested__something", "stuff")
        == f"missingtype{FIELD_SUBCLASS_SEPARATOR}nested__something:stuff"
    )

    assert (
        m("nested__something__gte", 3)
        == f"missingtype{FIELD_SUBCLASS_SEPARATOR}nested__something:[3 TO *]"
    )

    assert m(
        "nested__something__label",
        "dog",
        "ParentThing",
        nested_document_fields={"nested"},
    ) == (
        '{!parent which="*:* -_nest_path_:* +pysolaar_type:parent_thing"}'
        "(+_nest_path_:\/_doc +pysolaar_type:parent_thing__*__nested +something__label:dog)"
    )


def test_map_fqs_to_pysolr_fq():
    # Alias this for typing ease
    s = PySolaarSearchSet(pysolaar_type="Person", search="text:dog")
    assert s._map_fqs_to_pysolr_fq() == ["pysolaar_type:person"]

    ss = s.filter(thing="cat")
    assert ss._map_fqs_to_pysolr_fq() == [
        "pysolaar_type:person",
        f"person{FIELD_SUBCLASS_SEPARATOR}thing:cat",
    ]

    sss = ss.filter(number__lt=5)
    assert sss._map_fqs_to_pysolr_fq() == [
        "pysolaar_type:person",
        f"person{FIELD_SUBCLASS_SEPARATOR}thing:cat",
        f"person{FIELD_SUBCLASS_SEPARATOR}number:[* TO 5}}",
    ]


def test_setting_search_term():
    s = PySolaarSearchSet(pysolaar_type="Person", search="text:dog")
    assert s._search == "text:dog"
    ss = s.filter(thing="thong")
    assert ss._search == "text:dog"
