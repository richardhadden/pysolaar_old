import datetime
import re
from pysolaar.pysolaar import decode_id

import pytest

import pysolr
from pysolaar import __version__
from pysolaar import (
    PySolaar,
    PySolaarError,
    fix_output_values,
    encode_id,
    ID_SUBCLASS_SEPARATOR,
    FIELD_SUBCLASS_SEPARATOR,
    SOLR_TERM_REGEX,
)


""" Notes:

- For reasons of I've no idea, pytest only likes it if you define subclasses inline
  rather than just anywhere, or even in a fixture. Probably due to this daft intro-
  spection I'm doing.

- Tried for ages to get pytest_solr to work, but can't make it launch the binary, or
  connect from pysolr (or both); instead, using the solr() fixture running against
  a local Solr instance that's been manually set up with a test_solr core. (I'm running
  this using docker-compose; maybe I should include the docker-compose.yml file here?)
"""

PYSOLR_URL = "http://localhost:8983/solr/test_solr"


def test_version():
    assert __version__ == "0.7.0"


# Here we configure a PySolr client for testing...
# and also make sure we clean up properly after each test
@pytest.fixture(scope="module")
def solr():

    client = pysolr.Solr(PYSOLR_URL, always_commit=True)
    client.delete(q="*:*")
    yield client
    client.delete(q="*:*")  # Reset Solr by trashing everything


@pytest.fixture
def pySolaarClass():
    PySolaar._RESET()
    # Here, we configure PySolaar's "inner" PySolr client
    PySolaar.configure_solr(PYSOLR_URL, always_commit=True)
    # PySolaar._RESET()
    yield PySolaar
    PySolaar._RESET()


def test_solr(solr):
    """Test for my sanity between tests, that we can add
    docs to Solr and trash them all again
    """
    solr.add(
        [{"id": "thing", "text": "thingy"}, {"id": "otherthing", "text": "otherthingy"}]
    )
    result = solr.search(q="*:*")
    assert len(result) == 2
    solr.delete(q="*:*")
    result = solr.search(q="*:*")
    assert len(result) == 0


def test_reset_pysolaar(pySolaarClass):
    """Need some method to reset the subclasses of PySolaar between
    tests, so I added this _RESET() method for debugging purposes;
    here we confirm that it works
    """

    class SomeClass(pySolaarClass):
        def get_index_set(self):
            return []

    assert len(pySolaarClass.pysolaar_classes) == 1

    pySolaarClass._RESET()
    assert len(pySolaarClass.pysolaar_classes) == 0


def test_subclassing_adds_subclass_to_list(pySolaarClass):
    """ Test subclassing PySolaar adds subclass to list of subclasses  """

    class SomeTestClassOne(pySolaarClass):
        def get_index_set(self):
            return []

    assert pySolaarClass.pysolaar_classes == [SomeTestClassOne]


def test_subclass_must_implement_get_index_set_method(pySolaarClass):
    """Tests that subclassing PySolaar without declaring 'get_index_set'
    method raises NotImplementedError
    """
    with pytest.raises(NotImplementedError) as err:

        class SomeTestClassTwo(pySolaarClass):
            pass

    assert "needs to implement 'get_index_set' method" in str(err.value)
    assert "<SomeTestClassTwo>" in str(err.value)


def test_building_indexable_data(pySolaarClass):
    """Tests that the base class can iterate over its subclasses, calling their
    get_index_set methods, append the class name as a @type attribute to the
    dict.

    For now, we'll just build a class method on PySolaar to check this
    works.
    """

    class TestClassA(pySolaarClass):
        def get_index_set(self):
            return [
                {"text": "text one"},
                {"text": "text two"},
            ]

    class TestClassB(pySolaarClass):
        def get_index_set(self):
            return [
                {"text": "text three"},
                {"text": "text four"},
            ]

    assert list(pySolaarClass.get_data_to_index()) == [
        {
            "pysolaar_type": "test_class_a",
            f"test_class_a{FIELD_SUBCLASS_SEPARATOR}text": "text one",
        },
        {
            "pysolaar_type": "test_class_a",
            f"test_class_a{FIELD_SUBCLASS_SEPARATOR}text": "text two",
        },
        {
            "pysolaar_type": "test_class_b",
            f"test_class_b{FIELD_SUBCLASS_SEPARATOR}text": "text three",
        },
        {
            "pysolaar_type": "test_class_b",
            f"test_class_b{FIELD_SUBCLASS_SEPARATOR}text": "text four",
        },
    ]


def test_add_solr_configuration_as_args(pySolaarClass):
    """ Test we can pass through arbitrary arguments to the PySolr instance """
    pySolaarClass.configure_solr("URL")
    assert pySolaarClass._solr.url == "URL"

    pySolaarClass.configure_solr("URL2", timeout=9999)
    assert pySolaarClass._solr.url == "URL2"
    assert pySolaarClass._solr.timeout == 9999


def test_add_solr_configuration_as_pysolr_object(pySolaarClass):
    """ Don't need this now """
    pass


def test_raises_error_if_pysolr_not_configured():
    # Unconfigured!
    class Test(PySolaar):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"text": "text one"},
                {"text": "text two"},
            ]

    with pytest.raises(PySolaarError) as error:
        PySolaar.update()

    assert (
        str(error.value)
        == """You must call PySolaar.configure_solr(<URL>) with Solr URL before calling PySolaar.update() or running any searches."""
    )

    with pytest.raises(PySolaarError) as error:
        len(Test.search("text"))

    assert (
        str(error.value)
        == """You must call PySolaar.configure_solr(<URL>) with Solr URL before calling PySolaar.update() or running any searches."""
    )


def test_pysolaar_can_update_solr(pySolaarClass, solr):
    class TestClassA(pySolaarClass):
        def get_index_set(self):
            return [
                {"text": "text one"},
                {"text": "text two"},
            ]

    pySolaarClass.update()
    result = solr.search("*:*")
    assert len(result) == 2


def test_default_search_field(pySolaarClass, solr):
    class TestClass(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"text": "text one"},
                {"text": "text two"},
            ]

    pySolaarClass.update()

    assert (
        TestClass.search("one")._search
        == f"test_class{FIELD_SUBCLASS_SEPARATOR}text:one"
    )


def test_search_on_specific_field(pySolaarClass, solr):
    class TestClass(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"text": "text one", "something": "else"},
                {"text": "text two", "something": "or other"},
            ]

    pySolaarClass.update()

    assert (
        TestClass.search("something:other")._search
        == f"test_class{FIELD_SUBCLASS_SEPARATOR}something:other"
    )

    assert (
        TestClass.search("something:other AND text:one")._search
        == f"test_class{FIELD_SUBCLASS_SEPARATOR}something:other AND test_class{FIELD_SUBCLASS_SEPARATOR}text:one"
    )


def test_you_need_a_default_field_or_to_specify(pySolaarClass, solr):
    class TestClass(pySolaarClass):
        # No default search field specified

        def get_index_set(self):
            return [
                {"text": "text one", "something": "else"},
                {"text": "text two", "something": "or other"},
            ]

    pySolaarClass.update()

    with pytest.raises(PySolaarError) as err:
        TestClass.search("one")


def test_pysolaar_search(pySolaarClass, solr):
    class TestThing(pySolaarClass):
        default_search_field = "text_bit"

        def get_index_set(self):
            return [
                {"id": "one", "text_bit": "text one"},
                {"id": "two", "text_bit": "text two"},
            ]

    pySolaarClass.update()

    # Check searching for 'one' in text returns
    # only the first
    result = TestThing.search("one")
    assert len(result) == 1

    result = TestThing.search("text")
    assert len(result) == 2


def test_pysolaar_search_distinguishes_by_class(pySolaarClass, solr):
    class TestThing(pySolaarClass):
        def get_index_set(self):
            return [
                {"id": "one", "text": "text one"},
                {"id": "two", "text": "text two"},
            ]

    class OtherThing(pySolaarClass):
        def get_index_set(self):
            return [
                {"id": "three", "text": "text three"},
                {"id": "four", "text": "text four"},
            ]

    pySolaarClass.update()  # Maybe automate this?

    # Quick sanity check first...
    result = OtherThing.search("text:four")
    assert len(result) == 1

    # So this *should* be just things of type 'otherthing'
    result = OtherThing.search("text:text")
    assert len(result) == 2

    # And check we can pass through additional fqs
    result = OtherThing.search("text:text").filter(id="four")
    print(result._search)
    assert len(result) == 1


def test_id_replacement_in_manual_solr_queries(pySolaarClass):
    class OtherThing(pySolaarClass):
        def get_index_set(self):
            return [
                {"id": "three", "text": "text three"},
                {"id": "four", "text": "text four"},
            ]

    pySolaarClass.update()

    # Test when id is at start of search string
    result = OtherThing.search(f"id:four AND text:text")
    assert (
        result._search
        == f"id:other_thing{ID_SUBCLASS_SEPARATOR}four AND other_thing{FIELD_SUBCLASS_SEPARATOR}text:text"
    )
    assert len(result) == 1
    assert result.first()["id"] == "four"

    # and when it's after a space
    result = OtherThing.search("text:text AND id:four")
    assert (
        result._search
        == f"other_thing{FIELD_SUBCLASS_SEPARATOR}text:text AND id:other_thing{ID_SUBCLASS_SEPARATOR}four"
    )
    assert len(result) == 1

    # Let's check for dumb edge case where a field trivially ends in 'id'
    result = OtherThing.search("text:text AND aphid:four")
    assert (
        result._search
        == f"other_thing{FIELD_SUBCLASS_SEPARATOR}text:text AND other_thing{FIELD_SUBCLASS_SEPARATOR}aphid:four"
    )


@pytest.fixture
def notEvaluatedSearchObject(pySolaarClass):
    class TestThing(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"id": "one", "text": "text one"},
                {"id": "two", "text": "text two"},
            ]

    pySolaarClass.update()

    so = TestThing.search("one")
    return so


def test_result_not_evaluated_immediately(notEvaluatedSearchObject):
    so = notEvaluatedSearchObject
    assert not so._results


def test_calling_iter_evaluates_search_object(notEvaluatedSearchObject):
    so = notEvaluatedSearchObject
    assert not so._results

    for i in so:
        pass

    assert so._results


def test_getting_len_evaluates_search_object(notEvaluatedSearchObject):
    so = notEvaluatedSearchObject
    assert not so._results

    len(so)

    assert so._results


def test_calling_bool_evaluates_search_object(notEvaluatedSearchObject):
    so = notEvaluatedSearchObject
    assert not so._results

    if so:
        pass

    assert so._results


def test_calling_repr_evaluates_search_object(notEvaluatedSearchObject):
    so = notEvaluatedSearchObject
    assert not so._results

    repr(so)
    # assert "ExtendedResults object" in repr(so)
    # assert "TestThingSolrSearchObject" in repr(so)
    # assert "[('text:one', ['pysolaar_type=test_thing'])]" in repr(so)

    assert so._results


def test_getattr_evaluates_and_proxys_search_object(pySolaarClass):
    class TestOne(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"id": "one", "text": "text one"},
                {"id": "two", "text": "text two"},
            ]

    pySolaarClass.update()

    assert TestOne.search("one").first()["id"] == "one"


# TODO: id's for a class need to be prefaced with class name to ensure unique
#           - also, query function modified to ensure this works


# =================================================
# Above is really the functionality; now let's check
# it actually works with Solr by running random stuff


def test_models_can_have_different_things(pySolaarClass):
    class TestOne(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"id": "one", "text": "text one"},
                {"id": "two", "text": "text two"},
            ]

    class TestTwo(pySolaarClass):
        default_search_field = "blob"

        def get_index_set(self):
            return [
                {"id": "three", "blob": "text three"},
                {"id": "four", "blob": "text four"},
            ]

    pySolaarClass.update()

    assert len(TestOne.search("one")) == 1
    assert len(TestTwo.search("three")) == 1


def test_lots_of_results(pySolaarClass):
    class TestOne(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            # Generate some samples
            return [{"id": f"item-{i}", "text": f"text example {i}"} for i in range(20)]

    pySolaarClass.update()

    search = TestOne.search("example")

    assert search.hits == 20
    assert len(search) == 20

    # Check when iterating that we get more than 10 results
    i = 0
    for _ in search:
        i += 1

    assert i == 20


def test_search_by_dates(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "text one",
                    # For testing, we'll give this one a datetime.date...
                    "date": datetime.date(1984, 10, 12),
                },
                {
                    "id": "two",
                    "text": "text two",
                    # ... and this one a datetime.datetime
                    "date": datetime.datetime(1986, 10, 12),
                },
            ]

    pySolaarClass.update()

    result = TestItem.search("text").filter(date__gt="1982-01-01T00:00:00Z")
    assert len(result) == 2

    result = TestItem.search("text").filter(date__gt="1985-01-01T00:00:00Z")
    assert len(result) == 1

    result = TestItem.search("text").filter(date__gt="1987-01-01T00:00:00Z")
    assert len(result) == 0

    result = TestItem.search("text").filter(
        date__gt=datetime.datetime(1982, 1, 1, 0, 0, 0)
    )
    assert len(result) == 2

    result = TestItem.search("text").filter(date__gt=datetime.date(1982, 1, 1))
    assert len(result) == 2


def test_results(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"
        single_value_fields = ("text", "date")

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "text one",
                    # For testing, we'll give this one a datetime.date...
                    "date": datetime.date(1984, 10, 12),
                },
                {
                    "id": "two",
                    "text": "text two",
                    # ... and this one a datetime.datetime
                    "date": datetime.datetime(1986, 10, 12),
                },
            ]

    pySolaarClass.update()

    result = TestItem.search("text")

    first = result.first()

    # TODO: RUN FROM HERE: rename the fields...
    assert first["id"] == "one"
    assert first["text"] == "text one"

    assert first["date"] == datetime.datetime(1984, 10, 12)

    no_result = TestItem.search("bollocks")
    assert no_result.first() is None


def test_fix_output_values():
    # Modified this assertion to allow for single_value_fields specification
    assert fix_output_values(["one"], key="this", single_value_fields={"this"}) == "one"

    assert fix_output_values(["one", "two"]) != "one"
    assert fix_output_values(["one", "two"]) == ["one", "two"]

    assert fix_output_values("1984-10-12T00:00:00Z") == datetime.datetime(1984, 10, 12)
    assert fix_output_values(["1984-10-12T00:00:00Z", "1985-10-12T00:00:00Z"]) == [
        datetime.datetime(1984, 10, 12),
        datetime.datetime(1985, 10, 12),
    ]


# =============================================================================
# Implementing get_index_set as generator function (turns out it's easy)
# =============================================================================


def test_get_index_set_works_with_list_of_dicts(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "text one",
                },
                {
                    "id": "two",
                    "text": "text two",
                },
            ]

    pySolaarClass.update()

    assert len(TestItem.search("text")) == 2


def test_get_index_set_works_with_generator(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            yield from [
                {
                    "id": "one",
                    "text": "text one",
                },
                {
                    "id": "two",
                    "text": "text two",
                },
            ]

    pySolaarClass.update()

    assert len(TestItem.search("text")) == 2
    assert TestItem.search("text").first()["id"] == "one"


def test_ids_prefaced_by_subclass_in_solr(pySolaarClass):
    """The idea of this test is that having two docs with the same "id" will
    conflict in Solr, unless we add some distinguishing thing — in this case,
    the class name
    """
    single_field_values = ["text"]

    class TestItemOne(pySolaarClass):
        default_search_field = "text"
        single_value_fields = ("text",)

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "text one",
                },
                {
                    "id": "two",
                    "text": "text two",
                },
            ]

    class TestItemTwo(pySolaarClass):
        default_search_field = "text"
        single_value_fields = ("text",)

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "text three",
                },
                {
                    "id": "two",
                    "text": "text four",
                },
            ]

    pySolaarClass.update()

    test_one_results = TestItemOne.search("text")
    assert len(test_one_results) == 2

    test_two_results = TestItemTwo.search("text")
    assert len(test_two_results) == 2

    assert list(test_one_results)[0]["text"] == "text one"
    assert list(test_one_results)[1]["text"] == "text two"
    assert list(test_two_results)[0]["text"] == "text three"
    assert list(test_two_results)[1]["text"] == "text four"

    assert list(test_one_results)[0]["id"] == "one"
    assert list(test_one_results)[1]["id"] == "two"
    assert list(test_two_results)[0]["id"] == "one"
    assert list(test_two_results)[1]["id"] == "two"


def test_updating_subclass_only_updates_specific_subclass(pySolaarClass):
    class TestItemOne(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "text one",
                },
                {
                    "id": "two",
                    "text": "text two",
                },
            ]

    class TestItemTwo(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {
                    "id": "three",
                    "text": "text one",
                },
                {
                    "id": "four",
                    "text": "text two",
                },
            ]

    pySolaarClass.update()

    assert len(TestItemOne.search("text")) == 2
    # assert len(TestItemTwo.search("text")) == 0
    pySolaarClass._RESET()


def test_updating_does_work(pySolaarClass):
    class TestObject(pySolaarClass):
        default_search_field = "text"
        single_value_fields = ("text",)
        # This is just here so we can easily change the value
        # returned by get_index_set
        uploaded_once = False

        def get_index_set(self):
            if self.uploaded_once:
                return [
                    {
                        "id": "one",
                        "text": "text one UPDATED",
                    },
                ]
            else:
                self.uploaded_once = True
                return [
                    {
                        "id": "one",
                        "text": "text one",
                    },
                ]

    class TestObjectTwo(pySolaarClass):
        default_search_field = "text"
        single_value_fields = ("text",)
        # This is just here so we can easily change the value
        # returned by get_index_set

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "text one",
                },
            ]

    pySolaarClass.update()

    assert list(TestObject.search("text"))[0]["text"] == "text one"

    # Check that variable's actually updating
    assert TestObject.uploaded_once == True

    # Call update
    TestObject.update()

    # And check the text value has updated
    assert list(TestObject.search("text"))[0]["text"] == "text one UPDATED"

    assert list(TestObjectTwo.search("text"))[0]["text"] == "text one"


def test_update_single_object_fails_if_no_method_implemented(pySolaarClass):
    class ObOne(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [{"id": "one", "text": "something"}]

    pySolaarClass.update()

    with pytest.raises(PySolaarError):
        ObOne.update("one")


def test_update_single_object(pySolaarClass):
    class ObOne(pySolaarClass):
        default_search_field = "text"
        single_value_fields = ("text",)

        def get_index_set(self):
            return [{"id": "one", "text": "something"}]

        def get_single_object(self, key):
            return {"id": "one", "text": f"something else with key {key}"}

    pySolaarClass.update()

    result = ObOne.search("something")
    assert len(result) == 1
    assert result.first()["text"] == "something"

    ObOne.update("one")

    result = ObOne.search("something")
    assert len(result) == 1
    assert result.first()["text"] == "something else with key one"


def test_encode_id_helper():
    assert encode_id("SomeClass", "identifier1") == "some_class#_#_#_#_#identifier1"


def test_decode_id_helper():
    assert decode_id(f"some_class{ID_SUBCLASS_SEPARATOR}identifier1") == "identifier1"


def test_add_boost_to_object_works(pySolaarClass):
    class Thingy(pySolaarClass):
        default_search_field = "text"
        boost = {"text": 1}

        def get_index_set(self):
            return [{"id": "one", "text": "something"}]

    class OtherThingy(pySolaarClass):
        default_search_field = "text"
        boost = {"text": 2}

        def get_index_set(self):
            return [{"id": "one", "text": "something"}]

    assert pySolaarClass._get_boosts() == {
        f"thingy{FIELD_SUBCLASS_SEPARATOR}text": 1,
        f"other_thingy{FIELD_SUBCLASS_SEPARATOR}text": 2,
    }


def test_wildcard_searches(pySolaarClass):
    class Stuff(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [{"id": "one", "text": "something"}]

    pySolaarClass.update()

    # Sanity
    assert len(Stuff.search("somethUng")) == 0
    assert len(Stuff.search("something")) == 1

    assert len(Stuff.search("someth?ng")) == 1
    assert len(Stuff.search("some*")) == 1
    assert len(Stuff.search("bome*")) == 0

    assert len(Stuff.search("somethung~1")) == 1
    assert len(Stuff.search("sometiung~1")) == 0


def test_filter_method_on_pysolaar_class(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"
        single_value_fields = ("text",)

        def get_index_set(self):
            return [
                {"id": "one", "text": "something", "other": "one"},
                {"id": "two", "text": "something else", "other": "one"},
            ]

    pySolaarClass.update()

    result = TestItem.filter(id="one")
    assert len(result) == 1
    assert result.first()["text"] == "something"

    result = TestItem.filter(other="one")
    assert len(result) == 2


def test_all_method_on_pysolaar_class(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"id": "one", "text": "something", "other": "one"},
                {"id": "two", "text": "something else", "other": "one"},
            ]

    pySolaarClass.update()

    result = TestItem.all()
    assert len(result) == 2

    result = result.filter(id="two")
    assert len(result) == 1


def test_search_for_phrases(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"id": "one", "text": "something", "other": "one"},
                {"id": "two", "text": "something else", "other": "one"},
            ]

    pySolaarClass.update()

    result = TestItem.all()
    assert len(result) == 2

    result = TestItem.search('"something else"')
    assert len(result) == 1


def test_filter_by_url(pySolaarClass):
    """ This test """

    class TestItem(pySolaarClass):
        default_search_field = "something"

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "something",
                    "something": "http://www.site.net/one",
                },
                {
                    "id": "two",
                    "text": "something else",
                    "something": "http://www.site.net/two",
                },
            ]

    pySolaarClass.update()

    # So in a simple search, we can automatically escape the colon
    result = TestItem.search("http://www.site.net/one")
    assert len(result) == 1
    assert result.first()["id"] == "one"

    # But if you're going to provide a raw Solr search, need to escape
    # manually: for some reason, the double quotes and the escape of the
    # colon are required (too confusing, without writing a parser...)
    result = TestItem.search('something:"http\://www.site.net/one"')
    assert len(result) == 1
    assert result.first()["id"] == "one"

    # Filtering is also automatically escaped
    result = TestItem.filter(something="http://www.site.net/two")
    assert len(result) == 1
    assert result.first()["id"] == "two"


def test_solr_term_regex():
    import re

    matches = re.findall(SOLR_TERM_REGEX, "test:something")
    assert matches == ["test:"]

    matches = re.findall(SOLR_TERM_REGEX, "test:something AND other:different")
    assert matches == ["test:", "other:"]

    matches = re.findall(SOLR_TERM_REGEX, 'test:"some phrase" AND other:different')
    assert matches == ["test:", "other:"]


def test_count_matches(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {"id": "one", "text": "something", "other": "one"},
                {"id": "two", "text": "something else", "other": "one"},
            ]

    pySolaarClass.update()

    # Just check this works
    result = TestItem.filter(id="one")
    assert result.first()["id"] == "one"
    assert result._results

    res = TestItem.filter(id="one")
    assert not res._results

    assert res.count() == 1
    assert not res._results
    assert res._count == 1


def test_paginate(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "something",
                },
                {
                    "id": "two",
                    "text": "something",
                },
                {
                    "id": "three",
                    "text": "something",
                },
                {
                    "id": "four",
                    "text": "something",
                },
                {
                    "id": "five",
                    "text": "something",
                },
                {
                    "id": "six",
                    "text": "something",
                },
            ]

    pySolaarClass.update()

    res = TestItem.search("something")
    assert res.count() == 6
    assert not res._results  # Check we haven't actually done any result-getting yet

    first_page = res.paginate(start=0, n=2)

    assert len(first_page) == 2
    assert list(first_page)[0]["id"] == "one"
    assert list(first_page)[1]["id"] == "two"

    second_page = res.paginate(start=2, n=4)
    assert len(second_page) == 4

    for i, name in enumerate(["three", "four", "five", "six"]):
        assert list(second_page)[i]["id"] == name


def test_iterate(pySolaarClass):
    class TestItem(pySolaarClass):
        default_search_field = "text"

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "something",
                },
                {
                    "id": "two",
                    "text": "something",
                },
                {
                    "id": "three",
                    "text": "something",
                },
                {
                    "id": "four",
                    "text": "something",
                },
                {
                    "id": "five",
                    "text": "something",
                },
                {
                    "id": "six",
                    "text": "something",
                },
            ]

    pySolaarClass.update()

    # Iterate the results, and check they match items in list
    # (regardless of how n -- should just be an iterator)
    for doc, name in zip(
        TestItem.search("something").iterator(chunk_size=2),
        ["one", "two", "three", "four", "five", "six"],
    ):
        assert doc["id"] == name

    # Iterate results, this time grouping by batch
    for resultsset, nameset in zip(
        TestItem.search("something").iterator(chunk_size=2, group_chunks=True),
        [["one", "two"], ["three", "four"], ["five", "six"]],
    ):
        for doc, name in zip(resultsset, nameset):
            assert doc["id"] == name


def test_return_fields(pySolaarClass):
    class Something(pySolaarClass):
        default_search_field = "text"

        return_fields = [
            "field1",
            "field3",
        ]

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "field1": "something",
                    "field2": "something",
                    "field3": "something",
                    "text": "some text",
                },
            ]

    pySolaarClass.update()

    result = Something.search("some text")
    assert len(result) == 1

    f = result.first()

    assert "id" in f  # should always return the id ?
    assert "field1" in f
    assert "field3" in f
    assert "field2" not in f
    assert "text" not in f

    result = Something.filter(text="some text")
    assert len(result) == 1

    f = result.first()
    assert "id" in f  # should always return the id ?
    assert "field1" in f
    assert "field3" in f
    assert "field2" not in f
    assert "text" not in f

    result = Something.all()
    for r in result:
        print(r)

    f = result.first()
    assert "id" in f  # should always return the id ?
    assert "field1" in f
    assert "field3" in f
    assert "field2" not in f
    assert "text" not in f


def test_setting_single_value_options(pySolaarClass):
    class Crap(pySolaarClass):
        # Set this variable to automatically unpack single values from a list
        single_value_fields = ["single_type"]
        default_search_field = "text"

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "list_type": ["something", "something else"],
                    "single_type": "something",
                    "text": "text",
                },
            ]

    pySolaarClass.update()

    result = Crap.search("text")
    f = result.first()

    assert f["id"] == "one"
    assert f["list_type"] == ["something", "something else"]
    assert f["single_type"] == "something"


def test_return_fields(pySolaarClass):
    class Something(pySolaarClass):
        default_search_field = "text"

        return_fields = [
            "field1",
            "field3",
        ]

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "field1": "something",
                    "field2": "something",
                    "field3": "something",
                    "text": "some text",
                },
            ]

    pySolaarClass.update()

    result = Something.search("some text")
    assert len(result) == 1

    f = result.first()

    assert "id" in f  # should always return the id ?
    assert "field1" in f
    assert "field3" in f
    assert "field2" not in f
    assert "text" not in f

    result = Something.filter(text="some text")
    assert len(result) == 1

    f = result.first()
    assert "id" in f  # should always return the id ?
    assert "field1" in f
    assert "field3" in f
    assert "field2" not in f
    assert "text" not in f

    result = Something.all()
    for r in result:
        print(r)

    f = result.first()
    assert "id" in f  # should always return the id ?
    assert "field1" in f
    assert "field3" in f
    assert "field2" not in f
    assert "text" not in f


def test_group_return_fields(pySolaarClass):
    class Rubbish(pySolaarClass):
        default_search_field = "text"

        return_fields = {
            "default": (
                "one",
                "two",
                "three",
                "four",
            ),
            "just_one_two": (
                "one",
                "two",
            ),
        }

        def get_index_set(self):
            return [
                {
                    "id": "the_id",
                    "one": "one",
                    "two": "two",
                    "three": "three",
                    "four": "four",
                    "five": "five",
                    "six": "six",
                    "text": "text",
                }
            ]

    pySolaarClass.update()

    # sanity
    assert len(Rubbish.search("text")) == 1

    default_search = Rubbish.search("text").first()
    for field in ["one", "two", "three", "four", "id"]:
        assert field in default_search

    just_one_two_search = Rubbish.search("text").return_fields("just_one_two").first()
    for field in ["one", "two", "id"]:
        assert field in just_one_two_search

    for field in ["three", "four", "five", "six", "text"]:
        assert field not in just_one_two_search

    __all__search = Rubbish.search("text").return_fields("__all__").first()
    for field in ["one", "two", "three", "four", "five", "six", "text", "id"]:
        assert field in __all__search

    custom_search = Rubbish.search("text").return_fields(["one"]).first()
    assert "id" in custom_search
    assert "one" in custom_search
    for field in ["two", "three", "four", "five", "six", "text"]:
        assert field not in custom_search


def test_order_by(pySolaarClass):
    class Crepe(pySolaarClass):
        default_order_by = ["otherfield", "-field"]

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "field": 1,
                    "otherfield": 1,
                },
                {
                    "id": "two",
                    "field": 2,
                    "otherfield": 1,
                },
            ]

    pySolaarClass.update()

    first_first = Crepe.all().order_by("field")
    for should_be_id, result in zip(["one", "two"], first_first):
        assert should_be_id == result["id"]

    last_first = Crepe.all().order_by("-field")
    for should_be_id, result in zip(["two", "one"], last_first):
        assert should_be_id == result["id"]

    last_first = Crepe.all().order_by("otherfield", "-field")
    for should_be_id, result in zip(["two", "one"], last_first):
        assert should_be_id == result["id"]

    last_first = Crepe.all().order_by(["otherfield", "-field"])
    for should_be_id, result in zip(["two", "one"], last_first):
        assert should_be_id == result["id"]

    default_order = Crepe.all()
    for should_be_id, result in zip(["two", "one"], default_order):
        assert should_be_id == result["id"]


def test_nested_documents(pySolaarClass):
    class ParentThing(pySolaarClass):
        nested_document_fields = ("nesty", "fusty")
        # store_as_json = ("nesty",)

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "parent stuff",
                    "nesty": ChildThing.items(["one", "two"]),
                    "fusty": ChildThing.items(["one"]),
                },
                {"id": "two", "text": "yowl stuff", "fusty": ChildThing.items(["two"])},
            ]

    class ChildThing(pySolaarClass):
        single_value_fields = ("text", "date")

        def get_index_set(self):
            return [
                {
                    "id": "cOne",
                    "text": "child stuff",
                    "stuff": 3,
                },
                {
                    "id": "cTwo",
                    "text": "more infant crap",
                    "date": datetime.date(1998, 1, 1),
                },
            ]

        def get_single_object(self, id):
            if id == "one":
                return {
                    "id": "cOne",
                    "text": "child stuff",
                    "stuff": 3,
                }
            if id == "two":
                return {
                    "id": "cTwo",
                    "text": "more infant crap",
                    "date": datetime.date(1998, 1, 1),
                }

    pySolaarClass.update()

    r = ParentThing.search("text:parent", fl="*,[child]")

    assert r.first()["id"] == "one"
    print(r.first())
    assert len(r.first()["nesty"]) == 2
    for nesty_field, should_be_id in zip(r.first()["nesty"], ["cOne", "cTwo"]):
        assert nesty_field["id"] == should_be_id

    # Check we can get parent by searching on child
    r = ParentThing.filter(nesty__text="child")
    assert r.first()["id"] == "one"

    # Check we can get parents by child ID
    r = ParentThing.filter(nesty__id="cOne").first()["id"] == "cOne"

    # And check we can still do stuff on the
    assert ChildThing.search("text:infant").first()["id"] == "cTwo"
    assert ChildThing.filter(id="cTwo").first()["id"] == "cTwo"

    # Check lookup on nested by date
    r = ParentThing.filter(fusty__date__gt=datetime.datetime(1997, 1, 1))
    assert len(r) == 1

    r = ParentThing.filter(fusty__date__gt=datetime.datetime(1999, 1, 1))
    assert len(r) == 0


def test_json_fields(pySolaarClass, solr):
    class MyThing(PySolaar):
        store_as_json = ("nesty",)

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "parent stuff",
                    "nesty": [
                        {"whatever": "thing", "stuff": "what"},
                        {"other": "crap", "listy": ["thing", "thong", "thang"]},
                    ],
                },
            ]

    pySolaarClass.update()

    # Let's do a manual check to see that we're really storing stuff as json
    result = solr.search("my_thing______text:parent")
    assert list(result)[0]["my_thing______nesty"] == [
        '[{"whatever": "thing", "stuff": "what"}, {"other": "crap", "listy": ["thing", "thong", "thang"]}]'
    ]

    result = MyThing.search("text:parent").first()
    assert result["id"] == "one"

    assert result["nesty"] == [
        {"whatever": "thing", "stuff": "what"},
        {"other": "crap", "listy": ["thing", "thong", "thang"]},
    ]


def test_dict_fields(pySolaarClass):
    class Stuffing(pySolaarClass):
        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "thing": {
                        "url": "AURL",
                        "label": "what",
                        "date": datetime.datetime.now(),
                    },
                },
            ]

    pySolaarClass.update()

    assert Stuffing.filter(thing__url="AURL").first()["id"] == "one"


def test_nested_generator_of_docs(pySolaarClass):
    class ChildThing(pySolaarClass):
        single_value_fields = ("text", "date")

        def get_index_set(self):
            return [
                {
                    "id": "cOne",
                    "text": "child stuff",
                    "stuff": 3,
                },
                {
                    "id": "cTwo",
                    "text": "more infant crap",
                    "date": datetime.date(1998, 1, 1),
                },
            ]

        def get_single_object(self, id):
            yield from [
                {
                    "id": "cOne",
                    "text": "child stuff",
                    "stuff": 3,
                },
                {
                    "id": "cTwo",
                    "text": "more infant crap",
                    "date": datetime.date(1998, 1, 1),
                },
            ]

    class ParentThing(pySolaarClass):
        nested_document_fields = ("nesty",)
        # store_as_json = ("nesty",)

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "parent stuff",
                    "nesty": ChildThing.items(["one"]),
                },
                {
                    "id": "two",
                    "text": "parent two",
                    "nesty": ChildThing.items(["one"]),
                },
            ]

    pySolaarClass.update()
    assert len(ParentThing.filter(nesty__text="infant")) == 2


def test_nested_doc_with_nested_field(pySolaarClass):
    class ChildThing(pySolaarClass):
        single_value_fields = ("text", "date")

        def get_index_set(self):
            return [
                {
                    "id": "cOne",
                    "text": "child stuff",
                    "stuff": {
                        "label": "stufflabel",
                    },
                },
            ]

        def get_single_object(self, id):
            return (
                {
                    "id": "cOne",
                    "text": "child stuff",
                    "stuff": {
                        "label": "stufflabel",
                    },
                },
            )

    class ParentThing(pySolaarClass):
        nested_document_fields = ("nesty",)
        # store_as_json = ("nesty",)

        def get_index_set(self):
            return [
                {
                    "id": "one",
                    "text": "parent stuff",
                    "nesty": ChildThing.items(["one"]),
                },
            ]

    pySolaarClass.update()

    assert ParentThing.filter(nesty__stuff__label="stuffLabel").first()["id"] == "one"
