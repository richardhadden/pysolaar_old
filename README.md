PySolaar
========

A (maybe?) overkill extension of/wrapper round PySolr so that it works in a declarative (and slightly magic)
class-based way like Django Rest Framework Serializer/Pagination/etc. classes.


General idea
------------

- Looks kind of like Django Haystack
- Not tied to a model like Haystack
- Just declare your class with a `get_index_set` method that returns an iterable of dicts for documents (from a Django DB, or anywhere else for that matter)
- It's all loaded into Solr when you call `update` on PySolaar
- Then just query your models with `search`, `filter`, `all` (kind of like Django, but also not in significant ways). Query objects are lazily evaluated.
- Indexed field names are automatically prefixed with the model name to avoid clashes if you have more than one model
- Each doc should have a unique (for each model) `id` field (transparently prefixed with model name to avoid clashes)
- Use Django-like queries for ranges: e.g. `filter(number__lt=3)`
- Use Solr queries if you like.
- **But really, use Haystack unless there's a good reason not to.**


Info
----

n.b. the docker-compose.yml is there as an easy way of firing up
a Solr instance for test
couldn't make it work. (In the tests, there is a fixture which deletes
all the Solr content between tests; maybe there's a more efficient way
of doing this? Anyway, it works for my limited number of tests.)

Basic usage
===========


1. Import `PySolaar`
```python
from pysolaar import PySolaar
```
2. Declare a `PySolaar` subclass; set a `default_search_field` to one of the fields you'll index; declare a `get_index_set` method, returning an iterable of dicts.
```python
class SomeStuffToIndex(PySolaar):
    
    # Set a default field to search on the model
    default_search_field = "text"

    def get_index_set(self):
        # Return a list of dicts to index (need unique "id" field to avoid duplication)
        return [
            {"id": "doc1", "text": "Once upon a time there was a something or other"},
            {"id": "doc2", "text": "And also there was another thing"}
        ]
```
3. Call `PySolaar.update()`. This will grab data from each subclass's `get_index_set` function, automatically prefix all the fields with the subclass's name and load it all into Solr.
   
4. Run some queries:
```python
results = SomeStuffToIndex.search("another") # Searches on specified default_search_field
other_results = SomeStuffToIndex.filter(id="doc1") # Filter by some field
more_results = SomeStuffToIndex.search("was").filter(id="doc2") # Chain filters together
```
5. Queries are lazily evaluated. On evaluation, a `pysolr.Results` object is cached.
```python
results = SomeStuffToIndex.all()

for result in results: # Query evaluated here
    print(result)
```
6.  `.first()` can also called on a Results set to get the first item.
```python
results = SomeStuffToIndex.search("another")
first = results.first()
first["id"] # => "doc2" 
```

Further usage
=============

## Pagination

The `.paginate()` method can be used to get subsets of results.
```python
result = Dudes.search("Bob").paginate(n=10, start=10)
# Gets 10 results, with cursor at 10
```

## Count
`.count()` returns the number of matches directly from Solr, without getting any results (useful if also using `paginate`). By contrast, calling `len()` on a results set will evaluate the query, get all the results and then count them using Python. (It is possible call `count` after `paginate`, but this will return the total number of hits, not number on a given page; for the latter, use `len()`).

```python
count = Dudes.search("Bob").count() # => e.g. 5
```

## Iterator
`.iterator()` fetches results from Solr in whatever size batch you set, and then provides them as an iterator without caching the results (the same as `QuerySet.iterator` in Django).

```python
for doc in Dudes.search("Bob").iterator(chunk_size=10):
    print(doc)
```
The `chunk_size` kwarg only determines how many items are fetched from Solr in a single request. Otherwise, the above is equivalent to:
```python
for doc in Dudes.search("Bob"):
    print(doc)
```
To iterate over the chunks, use `group_chunks` kwargs:
```python
for chunk_of_docs in Dudes.search("Bob").iterator(chunk_size=10, group_chunks=True):
    for doc in chunk_of_docs:
        print(doc)
```

**n.b. `paginate`, `count` and `iterator` must be called on a search set — i.e. _after_ calling `search`, `filter` or `all` on your class.**


### Big datasets to load

If getting a lot of data from somewhere (e.g. Django) make `get_index_set` a generator.
```python
class SomePersonDataToIndex(PySolaar):
    def get_index_set(self):
        
        # Using the Django iterator to get 200 items at a time
        # from the database
        for p in Person.objects.iterator(chunk_size=200):
            person_data = {}
            person_data["id"] = p.pk + str(p.dob) 
            person_data["bio"] = p.biography
            yield person_data
```
Also, add a `max_chunk_size` kwarg to the `update()` method to control how many
docs are written to Solr at a time.
```python
PySolaar.update(max_chunk_size=200)
```
This can be the same size as the chunks you're pulling from the database, or not. It's all just
generators pulling data through and filling up the right size chunks at each stage.

### Update specific models and docs

```python
class SomePersonDataToIndex(PySolaar):

    # ... [as above]

    def get_update_object(self, key):
        # If you want to be able to update single docs,
        # set this function...
        p = Person.objects.filter(pk=key).first()
        person_data = {}
        person_data["id"] = p.pk + str(p.dob) 
        person_data["bio"] = p.biography
        return person_data

# Update every 'SomePersonDataToIndex' document
SomePersonDataToIndex.update()

# Or update a specific document with an identifier
SomePersonDataToIndex.update(123)
```
__n.b.__ the identifier for updating the index does not have to be the same as the `id` field. It can be whatever you need in order to recreate that document from your database. 

For instance, in the example above, the Solr `id` field is created by concatenating the Django `pk` and a date-of-birth field as a string. _But_ in order to rebuild that document if some underlying data changes, you only need the Django `pk`. 

For the same reason, it doesn't really matter whether you're updating an item that is already in Solr, or uploading a new one.

Notes/issues
======
- Boosting fields: add a `boost` value to the class:
    ```python
    class SomePersonDataToIndex(PySolaar):
        
        boost = {"last_name": 5, "first_name": 3}

        # ... [as above]
    ```
- Filtering on a text-based field, e.g. 
    ```python
    Dudes.filter(first_name="Charlie")
    ``` 
    will do a regular Solr lookup (i.e. does the whole first_name field contain the token "Charlie"), not
    match for equality (as you might think). It's really like Django's `first_name__contains="Charlie"`.
- The standard Django-like `number__lt`, `number__lte`, `number__gt`, `number__gte` filters can all be used as args to the `filter` method. This works with dates as well, given as either Python `datetime.date` or `datetime.datetime` objects (casting them to some Solr-friendly version is done in the background).
- The `search` method can take any Solr standard query syntax, e.g.:
    ```python
    Dudes.search("bio:wastrel~3 AND dob:[1976-03-06T23:59:59.999Z TO NOW]")
    ```
    _in theory_! At the moment, it uses regexes to replace field names in a query with its prefixed version, i.e. `bio:wastrel~3` is replaced with `dudes_____bio:wastrel~3`. This hasn't been very thoroughly tested yet.
- Don't name fields anything with five consecutive underscores as it will mess with this prefixing business!!!







To do:
=====

- add fields to return variable
- implement OrderBy...
- field-prefixing regex doesn't work with urls (I think it's the colon after http://)
- Fix pagination --- for next page start pos,  need to multiply page number by page length
- Filtering by ID that does not return a result should raise KeyError like Django..?

- Would it be useful to make each PySolaar subclass create its own Data class, so that it can do type validation and such like (rather than "result a list of dicts")? Something like this (using type annotations, validation functions, etc., etc.?) -- which could also be passed to the SearchObject as a validator for filter kwargs:

```python

## Attention! This hasn't been built!!

class SomethingToIndex(PySolaar):
    # Probably don't do this as MyPy will object!
    id: str = lambda id: re.match(r"doc\d", id) and id.startswith('d')
    text: str
    number: int
    date: datetime.datetime

    def get_index_set(self):
        
        return [
            self.Doc(
                id="doc1", 
                text="Once upon a time there was a something or other", 
                number=3
            ),
            self.Doc(
                id="doc2", 
                text="And also there was another thing", 
                number=4
            )
        ]

```
